/**
 * Created by Thanh on 8/27/2016.
 */

var game;
var PhomBoard;
var PhomPlayer;
var TLMNDLBoard;
var TLMNDLPlayer;

var gameCodeToBoardClassMap = {
    'pom': PhomBoard,
    'tnd': TLMNDLBoard
};

var gameCodeToPlayerClassMap = {
    'pom': PhomPlayer,
    'tnd': TLMNDLPlayer
};

var GameManager = cc.Class({

    getBoardClass: function getBoardClass(gameCode) {
        return gameCodeToBoardClassMap[gameCode];
    },

    getPlayerClass: function getPlayerClass(gameCode) {
        return gameCodeToPlayerClassMap[gameCode];
    }

});

GameManager.newInstance = function () {
    game = require('game');
    return new GameManager();
};

module.exports = GameManager;