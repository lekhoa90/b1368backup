/**
 * Created by Thanh on 8/25/2016.
 */

var game;

var SystemEventHandler = cc.Class({

    properties: {
        _eventListeners: {
            "default": {}
        }
    },

    ctor: function ctor() {
        game = require("game");
    },

    /**
     * Handle data sent from server
     *
     * @param {string} cmd - Command or request name sent from server
     * @param {object} data - Data sent according to request
     */
    handleData: function handleData(cmd, data) {
        var _this = this;

        game.async.series([function () {
            _this._callListener(cmd, data);
        }, function () {
            _this._currentScene && _this._currentScene.handleData(cmd, data);
        }]);
    },

    _registerAllSystemEventHandler: function _registerAllSystemEventHandler() {
        this.addSystemEventListener("test", function () {
            console.log("Test event");
        });
        //TODO
    },

    _removeAllSystemEventHandler: function _removeAllSystemEventHandler() {
        this._eventListeners = {};
    },

    /**
     * Game service will be call this listener has registered when data send from
     *
     * @param {string} key  - Key map with listener
     * @param {function} listener - Function handle data. Function must contain 2 parameters.
     * + cmd - {string} (the command of extension request or request name)
     * + data - {object} (The data sent from server)
     *
     */
    addSystemEventListener: function addSystemEventListener(key, listener) {
        if (key) {
            this._eventListeners[key] = listener instanceof Function ? listener : undefined;
        }
    },

    /**
     *
     * @param {string} key - Name mapped with listener
     */
    removeSystemEventListener: function removeSystemEventListener(key) {
        key && delete this._eventListeners[key];
    },

    _callListener: function _callListener(key, args) {
        var listener = this._eventListeners[key];
        var argArr = Array.prototype.slice.call(arguments, 1);
        listener && listener.apply(null, argArr);
    },

    _callListenerAsync: function _callListenerAsync(key, args) {
        var _this2 = this,
            _arguments = arguments;

        game.async.series([function (cb) {
            _this2._callListener(_arguments);
        }]);
    }

});

module.exports = SystemEventHandler;