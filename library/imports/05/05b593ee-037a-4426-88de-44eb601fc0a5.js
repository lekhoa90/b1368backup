/**
 * Created by Thanh on 8/23/2016.
 */

var game = require('game');
var Player = require('Player');
var SFS2X = require('SFS2X');
var utils = require('utils');

var PlayerManager = cc.Class({

    properties: {
        board: null,
        me: null,
        players: null,
        _idToPlayerMap: null,
        _nameToPlayerMap: null,
        maxPlayerId: 1
    },

    _init: function _init(board) {
        this.board = board;
        this._reset();
    },

    _reset: function _reset() {
        this.me = undefined;
        this.players = [];
        this._idToPlayerMap = {};
        this._nameToPlayerMap = {};
        this.maxPlayerId = 1;
    },

    initPlayers: function initPlayers(users) {
        var _this = this;

        users.forEach(function (user) {
            _this._insertPlayerFromUser(user);
        });

        this._update();
    },

    /**
     * This function will be update all feature of player when data change
     *
     * @private
     */
    _update: function _update() {

        this.me = this.findPlayer(game.context.getMySelf().getPlayerId(this.board.room));

        //TODO set board owner & master

        var maxPlayerId = 1;

        this.players.forEach(function (player) {

            maxPlayerId = Math.max(maxPlayerId, player.id);

            //TODO more action want to apply to player
        });

        this.maxPlayerId = maxPlayerId;
    },

    _updateMaxPlayerId: function _updateMaxPlayerId() {
        var maxPlayerId = 1;

        this.players.forEach(function (player) {
            maxPlayerId = Math.max(maxPlayerId, player.id);
        });

        this.maxPlayerId = maxPlayerId;
    },

    _insertPlayerFromUser: function _insertPlayerFromUser(user) {
        if (user.isPlayer()) {
            var player = Player.create(this.board, user);
            this._addPlayerToBoard(player);
        }
    },

    _addPlayerToBoard: function _addPlayerToBoard(player) {
        if (this.players.indexOf(player) < 0) {

            //TODO add player ui to board

            this.players.push(player);
        }
    },

    isSpectator: function isSpectator() {
        return !game.me || !this.me.isPlaying() && this.board.isPlaying();
    },

    isMySelfPlaying: function isMySelfPlaying() {
        return !this.isSpectator() && this.me.isPlaying();
    },

    isShouldMySelfReady: function isShouldMySelfReady() {
        return this.me && !this.me.isOwner() && !this.me.isReady();
    },

    findPlayer: function findPlayer(idOrName) {
        if (idOrName instanceof Number) {
            return this._idToPlayerMap[idOrName];
        } else {
            return this._nameToPlayerMap[idOrName];
        }
    },

    _findPlayerIndex: function _findPlayerIndex(playerId) {
        var playerIndex = -1;
        this.players.some(function (value, index) {
            if (value.id == playerId) {
                playerIndex = index;
                return true;
            }
        });
        return playerIndex;
    },

    _addPlayer: function _addPlayer(player) {
        var playerInMap = this._idToPlayerMap[player.id];
        var index = playerInMap && _findPlayerIndex(playerInMap.id);

        if (index && index >= 0) {
            this.players[index] = player;
        } else {
            this.players.push(player);
        }

        this._idToPlayerMap[player.id] = player;
        this._nameToPlayerMap[player.user.name] = player;
        this._update();
    },

    _removePlayer: function _removePlayer(player) {
        var _this2 = this;

        this.players.some(function (value, i, arr) {
            if (value.id == player.id) {

                _this2.players.splice(i, 1);
                delete _this2._idToPlayerMap[player.id];
                delete _this2._idToPlayerMap[player.user.name];

                _this2._update();

                return true;
            }
        });
    },

    _replaceUser: function _replaceUser(player, newId) {

        var oldUser = player.user;
        if (oldUser == null) {
            return;
        }

        var userObj = [];
        userObj.push(newId);
        userObj.push(oldUser.name);
        userObj.push(oldUser.privilegeId);
        userObj.push(player.id);
        userObj.push(this.__getUserVariablesData(oldUser));

        var newUser = SFS2X.Entities.SFSUser.fromArray(userObj, this.board.room);
        newUser._setUserManager(game.service.client.userManager);

        this.board.room._removeUser(oldUser);

        game.service.client.userManager._removeUser(oldUser);
        game.service.client.userManager._addUser(newUser);

        this.board.room._addUser(newUser);
        player.setUser(newUser);
    },

    countPlayingPlayers: function countPlayingPlayers() {
        var _this3 = this;

        var count = 0;
        this.players.forEach(function (player) {
            if ((_this3.board.isPlaying() || _this3.board.isStarting()) && player.isPlaying()) count++;
        });

        return count;
    },

    getPlayerSeatId: function getPlayerSeatId(playerId) {
        return this.board.positionManager.getPlayerSeatId(playerId);
    },

    getPlayerPosition: function getPlayerPosition(playerId) {
        return this.board.positionManager.getPlayerSeatPosition(playerId);
    },

    onBoardMinBetChanged: function onBoardMinBetChanged() {
        this.players.forEach(function (player) {
            !player.isOwner() && player.resetReadyState();
        });
    },

    onBoardMasterChanged: function onBoardMasterChanged(master) {
        this.players.forEach(function (player) {
            return player.onBoardMasterChanged();
        });
    },

    onBoardOwnerChanged: function onBoardOwnerChanged(owner) {
        this.players.forEach(function (player) {
            return player.onBoardOwnerChanged();
        });
    },

    playerOnBoardBegin: function playerOnBoardBegin(data) {
        this.players.forEach(function (player) {
            return player.onBoardBegin(data);
        });
    },

    playerOnBoardStarting: function playerOnBoardStarting(data) {
        this.players.forEach(function (player) {
            return player.onBoardStarting(data);
        });
    },

    playerOnBoardStarted: function playerOnBoardStarted(data) {
        this.players.forEach(function (player) {
            return player.onBoardStarted(data);
        });
    },

    playerOnBoardPlaying: function playerOnBoardPlaying(data) {
        this.players.forEach(function (player) {
            return player.onBoardPlaying(data);
        });
    },

    playerOnBoardEnd: function playerOnBoardEnd(data) {
        this.players.forEach(function (player) {
            return player.onBoardEnd(data);
        });
    },

    playerOnPlayerRejoin: function playerOnPlayerRejoin(playerIds, remainCardSizes, data) {
        var _this4 = this;

        playerIds && playerIds.forEach(function (id, i) {
            var player = _this4.findPlayer(playerIds[i]);
            player.onRejoin(remainCardSizes[i], data);
        });
    },

    onPlayerToSpectator: function onPlayerToSpectator(user) {
        this.players.forEach(function (player) {
            player.onPlayerToSpectator(user);
        });
    },

    onSpectatorToPlayer: function onSpectatorToPlayer(user) {
        this.players.forEach(function (player) {
            player.onSpectatorToPlayer(user);
        });
    },

    onPlayerLeaveBoard: function onPlayerLeaveBoard(playerOrIdOrName) {
        var leaveBoardPlayer = playerOrIdOrName instanceof Player ? playerOrIdOrName : this.findPlayer(playerOrIdOrName);

        if (leaveBoardPlayer) {
            if (leaveBoardPlayer.isMe()) {
                leaveBoardPlayer.stopTimeLine();
            } else {
                if (leaveBoardPlayer.isMaster()) {
                    this.board.setMaster(null);
                }

                this._removePlayer(leaveBoardPlayer);
            }
        }
    },

    _shouldLeaveBoardImmediately: function _shouldLeaveBoardImmediately(player) {
        if (player && player.hasOwnProperty('isTurn') && typeof player.isTurn == "function") {
            return !(this.board.isPlaying() && player.isTurn());
        } else {
            return !this.board.isPlaying();
        }
    },

    onUserEnterRoom: function onUserEnterRoom(user, room) {
        if (user && user.isPlayer() && !this.findPlayer(user.getPlayerId(room))) {

            var newPlayer = this._addPlayer(user);

            var boardState = this.board.isPlaying() || this.board.isStarting() ? game['const'].boardState.PLAYING : this.board.isBegin() ? game['const'].boardState.BEGIN : this.board.isReady() ? game['const'].boardState.READY : this.board.isEnding() ? game['const'].boardState.ENDING : undefined;

            if (boardState) {
                newPlayer.applyBoardState(boardState);
            }

            return true;
        }
    },

    onPlayerMessage: function onPlayerMessage(sender, message) {
        this.players.some(function (player) {
            if (player.name === sender.name) {
                player.say(message);
            }
        });
    },

    onMyselfRejoinGame: function onMyselfRejoinGame(resObj) {
        if (this.isMySelfPlaying()) {
            //TODO
        }
    },

    onPlayerReEnterGame: function onPlayerReEnterGame(playerId, newUserId) {
        var player = this.findPlayer(playerId);
        if (player) {
            this._replaceUser(player, newUserId);
        }
    },

    handlePlayer: function handlePlayer(playerId, cmd, data) {}

});

PlayerManager.newInstance = function (board) {
    var instance = new PlayerManager();
    instance._init(board);
    return instance;
};

module.exports = PlayerManager;