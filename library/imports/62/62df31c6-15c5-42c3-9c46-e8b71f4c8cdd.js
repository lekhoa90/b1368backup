var Player = cc.Class({
    "extends": cc.Component,

    properties: {
        id: 0,
        user: null,
        inited: false
    },

    init: function init(user) {
        //TODO

        this.inited = true;
    },

    onLoad: function onLoad() {},

    update: function update(dt) {},

    isOwner: function isOwner() {},

    isMaster: function isMaster() {},

    setUser: function setUser(user) {
        this.user = user;
    },

    /**
     * Show message player want to say
     * @param message
     */
    say: function say(message) {},

    applyBoardState: function applyBoardState(boardState) {
        //TODO
    },

    isMe: function isMe() {},

    stopTimeLine: function stopTimeLine() {},

    startTimeLine: function startTimeLine(timeInSeconds) {},

    onSpectatorToPlayer: function onSpectatorToPlayer(user) {},

    onPlayerToSpectator: function onPlayerToSpectator(user) {},

    onRejoin: function onRejoin(remainCardCount, data) {},

    onBoardBegin: function onBoardBegin(data) {},

    onBoardStarting: function onBoardStarting(data) {},

    onBoardStarted: function onBoardStarted(data) {},

    onBoardPlaying: function onBoardPlaying(data) {},

    onBoardEnd: function onBoardEnd(data) {},

    resetReadyState: function resetReadyState() {},

    onBoardMasterChanged: function onBoardMasterChanged(master) {},

    onBoardOwnerChanged: function onBoardOwnerChanged(owner) {}

});

Player.create = function (board, user) {
    var playerClass = game.manager.getPlayerClass(board.gameCode);

    if (playerClass) {
        var player = new playerClass();
        player.init(user);
        return player;
    }
};

module.exports = Player;