/**
 * Created by Thanh on 8/23/2016.
 */

var game = require('game');

var GameService = {
    properties: {
        client: {
            "default": null
        },

        _eventCallbacks: {
            "default": {}
        },

        _eventScopes: {
            "default": {}
        }
    },

    ctor: function ctor() {
        this._initSmartFoxClient();
    },

    _initSmartFoxClient: function _initSmartFoxClient() {

        var config = {};
        config.zone = game.config.zone;
        config.debug = game.config.debug;
        config.useSSL = game.config.useSSL;

        this.client = new SFS2X.SmartFox(config);
        this.client.setClientDetails("MOZILLA", "1.0.0");

        this._registerSmartFoxEvent();
    },

    __testConnection: function __testConnection() {
        var _this = this;

        this.connect(function (success) {
            console.debug("success: " + success);
            if (success) {
                _this.login("crush1", "1234nm", function (error, result) {
                    if (result) {
                        console.debug("Logged in as " + game.context.getMySelf().name);
                    }

                    if (error) {
                        console.debug("Login error: ");
                        console.debug(error);
                    }
                });
            }
        });
    },

    _registerSmartFoxEvent: function _registerSmartFoxEvent() {

        this._removeSmartFoxEvent();

        this.addEventListener(SFS2X.SFSEvent.LOGIN, this._onLogin);
        this.addEventListener(SFS2X.SFSEvent.LOGIN_ERROR, this._onLoginError);
        this.addEventListener(SFS2X.SFSEvent.CONNECTION, this._onConnection);
        this.addEventListener(SFS2X.SFSEvent.CONNECTION_LOST, this._onConnectionLost);
        this.addEventListener(SFS2X.SFSEvent.CONNECTION_RESUME, this._onConnectionResume);
        this.addEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, this._onExtensionEvent);

        console.debug("Registered SmartFox event");
    },

    _removeSmartFoxEvent: function _removeSmartFoxEvent() {
        this.removeEventListener(SFS2X.SFSEvent.LOGIN, this._onLogin);
        this.removeEventListener(SFS2X.SFSEvent.LOGIN_ERROR, this._onLoginError);
        this.removeEventListener(SFS2X.SFSEvent.CONNECTION, this._onConnection);
        this.removeEventListener(SFS2X.SFSEvent.CONNECTION_LOST, this._onConnectionLost);
        this.removeEventListener(SFS2X.SFSEvent.CONNECTION_RESUME, this._onConnectionResume);
        this.removeEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, this._onExtensionEvent);
    },

    _onConnection: function _onConnection(event) {
        console.debug("_onConnection");
        console.debug(event);

        this._callCallback(SFS2X.SFSEvent.CONNECTION, event.success);
    },

    _onConnectionLost: function _onConnectionLost(event) {
        console.debug("_onConnectionLost");
        console.debug(event);

        // game.system.loadScene(game.const.scene.LOGIN_SCENE);
    },

    _onConnectionResume: function _onConnectionResume(event) {
        console.debug("_onConnectionResume");
        console.debug(event);
        //TODO
    },

    _onExtensionEvent: function _onExtensionEvent(event) {
        console.debug("_onExtensionEvent");
        console.debug(event);

        if (this._hasCallback(event.cmd)) {
            this._callCallbackAsync(event.cmd, event.params);
        } else {
            game.system.handleData(event.cmd, event.params, event);
        }

        // game.async.series([
        //     () => {this._callCallback(event.cmd, event.params)},
        //     () => {game.system.handleData(event.cmd, event.params)}
        // ]);
    },

    _onLogin: function _onLogin(event) {
        console.debug("_onLogin");
        console.debug(event);

        this._callCallback(SFS2X.SFSEvent.LOGIN, undefined, event.data);
    },

    _onLoginError: function _onLoginError() {
        console.debug("_onLoginError");
        console.debug(event);

        this._callCallback(SFS2X.SFSEvent.LOGIN, event.data);
    },

    _sendRequest: function _sendRequest(request) {
        this.client.send(request);
    },

    _callCallback: function _callCallback(key, args) {
        var cb = this._getCallback(key);
        var argArr = Array.prototype.slice.call(arguments, 1);
        cb && cb.apply(null, argArr);
    },

    _callCallbackAsync: function _callCallbackAsync(key, args) {
        var _this2 = this,
            _arguments = arguments;

        game.async.series([function (callback) {
            _this2._callCallback.apply(_this2, _arguments);
        }]);
    },

    _hasCallback: function _hasCallback(key) {
        return this._eventCallbacks.hasOwnProperty(key);
    },

    _getCallback: function _getCallback(key) {
        return this._eventCallbacks[key];
    },

    _addCallback: function _addCallback(key, cb, scope) {
        this._eventCallbacks[key] = cb instanceof Function ? cb : undefined;
        this._addCommandToScope(key, scope);
    },

    addEventListener: function addEventListener(eventType, handleFunc) {
        this.client.addEventListener(eventType, handleFunc, this);
    },

    removeEventListener: function removeEventListener(eventType, handleFunc) {
        this.client.removeEventListener(eventType, handleFunc, this);
    },

    /**
     * Current Smart Fox Client
     *
     * @returns {SFS2X.SmartFox}
     */
    getClient: function getClient() {
        return this.client;
    },

    /**
     * Connect to server game with default host & port configuration
     * @param cb
     */
    connect: function connect(cb) {
        this.disconnect();

        this._addCallback(SFS2X.SFSEvent.CONNECTION, cb);

        console.debug("Connecting to: " + game.config.host + ":" + game.config.port);

        this.client.connect(game.config.host, game.config.port);
    },

    /**
     * Disconnect to game server
     */
    disconnect: function disconnect() {
        if (this.client.isConnected()) {
            this.client.disconnect();
        }
    },

    /**
     * Disconnect to game server and go to Login Screen
     */
    logout: function logout() {
        disconnect();
        game.system.loadScene(game["const"].scene.LOGIN_SCENE);
    },

    /**
     * @param {string} username
     * @param {string} password
     * @param {function} cb
     */
    login: function login(username, password, cb) {
        var data = {};
        data[Keywords.IS_REGISTER] = false;
        data[Keywords.PASSWORD] = password;
        data[Keywords.APP_SECRET_KEY] = "63d9ccc8-9ce1-4165-80c8-b15eb84a780a";

        this._addCallback(SFS2X.SFSEvent.LOGIN, cb);

        this._sendRequest(new SFS2X.Requests.System.LoginRequest(username, password, data, game.config.zone));
    },

    /**
     * @param {object} options  - Object data want to send via ExtensionRequest or other instance of RequestObject:
     *      + cmd: Command
     *      + data: Param object going to send to server
     *      + room: Specially room scope, null or undefined if is global scope (zone scope in smart fox)
     *
     * @param {function} cb - Callback function on server responses
     *
     */
    send: function send(options, cb, scope) {

        if (!options) return;

        if (options instanceof SFS2X.Requests._BaseRequest) {
            this._sendRequest(options);
        } else {
            var cmd = options.cmd;
            if (cmd) {
                this._addCallback(cmd, cb, scope);
                this._sendRequest(new SFS2X.Requests.System.ExtensionRequest(cmd, options.data || {}, options.room));
            }
        }
    },

    _addCommandToScope: function _addCommandToScope(key, scope) {
        if (key && scope) {
            var keys = this._eventScopes[scope] || {};
            keys[key] = "";
            this._eventScopes[scope] = keys;
        }
    },

    /**
     * Remove all callback mapped with key
     * @param {string} key
     */
    removeCallback: function removeCallback(key) {
        key && delete this._eventCallbacks[key];
    },

    /**
     *
     * @param {string} scope
     */
    removeAllCallback: function removeAllCallback(scope) {
        var _this3 = this;

        var eventCmdObj = scope && this._eventScopes[scope];
        if (eventCmdObj) {
            var keys = Object.keys(eventCmdObj);
            keys.forEach(function (key) {
                delete _this3._eventCallbacks[key];
            });
        }

        scope && delete this._eventScopes[scope];
    }

};

module.exports = GameService;