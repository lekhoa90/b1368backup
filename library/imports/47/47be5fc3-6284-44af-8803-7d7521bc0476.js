/**
 * Created by Thanh on 8/26/2016.
 */

var game = require("game");

game["const"].boardState = {};

game["const"].boardState.BOARD_STATE_PLAY_FAKED = 100;
game["const"].boardState.BOARD_STATE_READY = 1;
game["const"].boardState.BOARD_STATE_END = 2;
game["const"].boardState.BOARD_STATE_TURN_BASE_TRUE_PLAY = 3;
game["const"].boardState.BOARD_STATE_ARRANGE_CARD = 4;
game["const"].boardState.BOARD_STATE_BAO_XAM = 5;
game["const"].boardState.BOARD_STATE_FACE_DOWN = 5;
game["const"].boardState.BOARD_STATE_BET = 5;
game["const"].boardState.BOARD_STATE_DOWN = 6;
game["const"].boardState.BOARD_STATE_SPIN = 6;
game["const"].boardState.EXPLODING_RING_SPRITE_TAG = 20;
game["const"].boardState.TIMELINE_SPRITE_ZORDER = 20;
game["const"].boardState.CENTER_Y_PADDING_BOTTOM = 28;
game["const"].boardState.READY = 1;
game["const"].boardState.ENDING = 2;
game["const"].boardState.STATE_BAO_XAM = 5;
game["const"].boardState.STATE_TURN_BASE_TRUE_PLAY = 3;
game["const"].boardState.STATE_FACE_DOWN = 5;
game["const"].boardState.STATE_WAIT_TURN = 4;
game["const"].boardState.STATE_BET = 5;
game["const"].boardState.STATE_DOWN = 6;
game["const"].boardState.BOARD_STATE_SPIN = 6;
game["const"].boardState.INITED = -1;
game["const"].boardState.STATE_WAIT = -2;
game["const"].boardState.BEGIN = -3;
game["const"].boardState.STARTING = -4;
game["const"].boardState.PLAYING = -5;
game["const"].boardState.STATE_REJOIN = -6;
game["const"].boardState.STARTED = -7;
game["const"].boardState.HISTORY_KEYWORD_ENDTIME = "e";
game["const"].boardState.HISTORY_KEYWORD_DETAIL = "d";