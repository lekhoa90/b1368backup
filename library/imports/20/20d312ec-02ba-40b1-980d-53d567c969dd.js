/**
 * Created by Thanh on 8/23/2016.
 */

var PositionManager = cc.Class({

    properties: {
        board: null
    },

    _init: function _init(board) {
        this.board = board;
    },

    getPosition: function getPosition(playerId, gameCode) {},

    getPlayerSeatPosition: function getPlayerSeatPosition(playerId) {},

    getPlayerSeatPosition: function getPlayerSeatPosition(playerId) {}
});

PositionManager.newInstance = function (board) {
    var instance = new PositionManager();
    instance._init(board);
    return instance;
};

module.exports = PositionManager;